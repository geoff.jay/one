/*******************************************************************
| Game class file
*******************************************************************/
#include "Game.h"

Game::Game(GLuint width, GLuint height)
	: State(GAME_ACTIVE), Keys(), Width(width), Height(height)
{

}

Game::~Game()
{

}

void Game::gameInit()
{


}

void Game::loopUpdate(GLfloat dt)
{

}


void Game::loopProcessInput(GLfloat dt)
{

}

void Game::loopRender()
{

}
