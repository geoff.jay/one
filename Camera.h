#pragma once

// Standard includes
//#include <vector>

// GL includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Define abstract camera movement operations
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UP,
    DOWN
};

// Default camera values
const GLfloat YAW         = 0.0f;
const GLfloat PITCH       = -25.0f;
const GLfloat SPEED       = 40.0f;
const GLfloat SENSITIVITY = 0.15f;
const GLfloat ZOOM        = 1.0f;   // 1 radians
const GLfloat ORBITRADIUS = 50.0f;

// Abstract camera class that processes input, calculates Euler angles and appropriate glm vectors and matrices
class Camera
{
public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    // Euler angles
    GLfloat Yaw;
    GLfloat Pitch;
    // Camera options
    GLfloat MovementSpeed;
    GLfloat MouseSensitivity;
    GLfloat Zoom;
    GLfloat OrbitRadius;
    // Attach to player
    // Player* player;

    // Constructor: vector style input
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH, GLfloat orbitradius = ORBITRADIUS) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        this->Position = position;
        this->WorldUp = up;
        this->Yaw = yaw;
        this->Pitch = pitch;
        this->OrbitRadius = orbitradius;
        this->updateCameraVectors();
    }
    // Constructor: scalar style input
    Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        this->Position = glm::vec3(posX, posY, posZ);
        this->WorldUp = glm::vec3(upX, upY, upZ);
        this->Yaw = yaw;
        this->Pitch = pitch;
        this->updateCameraVectors();
    }

    glm::mat4 setCameraPosition(Player player) {
        // get camera to follow player
        // start by setting camera to be in line with player orientation
        this->Yaw = player.Yaw;
        this->Pitch = player.Pitch;
        // this->Right = player.Right;
        // Move camera back from player
        // GLfloat dx = this->OrbitRadius * cos(glm::radians(this->Yaw + player.Yaw)) * cos(glm::radians(this->Pitch + player.Pitch/1.2));
        // GLfloat dy = this->OrbitRadius * sin(glm::radians(this->Pitch + player.Pitch/1.2));
        // GLfloat dz = this->OrbitRadius * sin(glm::radians(this->Yaw + player.Yaw)) * cos(glm::radians(this->Pitch + player.Pitch/1.2));
        GLfloat dx = this->OrbitRadius * cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        GLfloat dy = this->OrbitRadius * sin(glm::radians(this->Pitch));
        GLfloat dz = this->OrbitRadius * sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        this->Position = player.Position - glm::vec3(dx,dy,dz);
        // this->Position = (player.Position + 30.0f*player.Front + 30.0f*player.Up) - glm::vec3(dx,dy,dz);
        // orient camera to look at player
        this->Front = glm::normalize(player.Position - this->Position);
        // Right
        this->Right = glm::normalize(glm::cross(this->Front, player.Up));
        // Up
        this->Up    = glm::normalize(glm::cross(this->Right, this->Front));

        return glm::lookAt(this->Position, this->Position + this->Front, this->WorldUp);

    }

    // Compute and return view matrix
    glm::mat4 getViewMatrix() {
        // Apply rotations; update Front/Right/Up vectors
        this->updateCameraVectors();

        // this->setCameraPosition();
        return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
    }

    // Process keyboard input
    void processKeys(Camera_Movement direction, GLfloat deltaTime) {
        GLfloat velocity = this->MovementSpeed * deltaTime;
        if (direction == FORWARD)
            this->Position += this->Front * velocity;
        if (direction == BACKWARD)
            this->Position -= this->Front * velocity;
        if (direction == LEFT)
            this->Position -= this->Right * velocity;
        if (direction == RIGHT)
            this->Position += this->Right * velocity;
        if (direction == UP)
            this->Position += this->Up * velocity;
        if (direction == DOWN)
            this->Position -= this->Up * velocity;
    }

    // Process mouse input
    void processMouseMove(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true) {
        xoffset *= this->MouseSensitivity;
        yoffset *= this->MouseSensitivity;

        // this->Yaw   += xoffset;
        this->Yaw   = glm::mod(Yaw + xoffset, 360.f);
        this->Pitch += yoffset;

        // Make sure the screen doesn't flip at Pitch limits
        if (constrainPitch) {
            if (this->Pitch > 89.0f) this->Pitch = 89.0f;
            if (this->Pitch < -89.0f) this->Pitch = -89.0f;
        }

        // Apply rotations; update Front/Right/Up vectors
        this->updateCameraVectors();
    }

    // Process mouse scroll-wheel input
    // 1deg = 0.01745rad, 45deg = 0.7854
    void processMouseScroll(GLfloat yoffset) {
        // if (this->Zoom >= 0.1745f && this->Zoom <= 1.7854f)
        //   this->Zoom -= 0.1 * yoffset;
        // if (this->Zoom <= 0.1745f)
        //   this->Zoom = 0.1745f;
        // if (this->Zoom >= 1.7854f)
        //   this->Zoom = 1.7854f;
        if (this->OrbitRadius >= 10.0f && this->OrbitRadius <= 100.0f)
        this->OrbitRadius -= 10.0f * yoffset;
        if (this->OrbitRadius <= 10.0f)
        this->OrbitRadius = 10.0f;
        if (this->OrbitRadius >= 100.0f)
        this->OrbitRadius = 100.0f;
    }

private:
    // Calculate Camera view vectors from updated Euler angles
    void updateCameraVectors() {
        // GLfloat dx = this->OrbitRadius * cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        // GLfloat dy = this->OrbitRadius * sin(glm::radians(this->Pitch));
        // GLfloat dz = this->OrbitRadius * sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        // this->Position = glm::vec3(dx, dy, dz);
        // Front
        glm::vec3 front;
        front.x = cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        front.y = sin(glm::radians(this->Pitch));
        front.z = sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        this->Front = glm::normalize(front);
        // this->Front = player.Front;
        // Right
        this->Right = glm::normalize(glm::cross(this->Front, this->WorldUp));
        // Up
        this->Up    = glm::normalize(glm::cross(this->Right, this->Front));
    }
};
