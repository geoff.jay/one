#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Renderer
{
public:
  Renderer(void);       // Constructor
  void createDisplay(){  // init Function
    // GLFW Initialization
    glfwInit();
    // Set GLFW options
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create the game window object
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Game Window", NULL, NULL);
    if (window == NULL) {
      printf("Failed to create GLFW Window");
      glfwTerminate();
      return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetWindowPos(window, 100, 100);

    // Set cursor options
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Register callback function for keypress and mouse
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // Set GLEW for modern OpenGL bindings and initialize
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
      printf("Failed to initialize GLEW");
      return -1;
    }

    // Define viewport dimensions
    //glfwGetFramebufferSize(window, WIDTH, HEIGHT);
    glViewport(0, 0, WIDTH, HEIGHT);

    // Set up OpenGL options
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    //glEnable(GL_CULL_FACE);
  }

  void updateDisplay(){

  }

  void closeDisplay(){

  }

}
