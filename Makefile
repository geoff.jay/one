# OBJS : compile target
OBJS = main.cpp Game.cpp

# CC : compiler
CC = g++
#CC = x86_64-w64-mingw32-g++

# COMPILER_FLAGS : compiler options
# -w suppresses all warnings
COMPILER_FLAGS = -w

# LINKER_FLAGS : required libraries
LINKER_FLAGS = -lassimp -lSOIL -lglfw -lGL -lGLEW -std=c++11

# OBJ_NAME : name of executable
OBJ_NAME = hello
#OBJ_NAME = hello.exe

# compile commands
all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)

clean :
	@echo Cleaning up binaries and object files...
	rm $(OBJ_NAME) *.o
	@echo ... done.

debug : $(OBJS)
	@echo Running in debug mode.
		$(CC) $(OBJS) -g $(LINKER_FLAGS) -o $(OBJ_NAME)
