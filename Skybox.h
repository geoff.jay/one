#include <GL/glew.h>
#include <vector>
#include <SOIL/SOIL.h>

class Skybox
{
public:
  std::vector<GLfloat> vertices;
  std::vector<const GLchar*> faces;
  GLuint cubemapTexture;
  // std::vector<GLuint> indices;

  // Constructor
  Skybox() {
    GLfloat skyboxVertices[] = {
      // Positions
      -1.0f,  1.0f, -1.0f,
      -1.0f, -1.0f, -1.0f,
      1.0f, -1.0f, -1.0f,
      1.0f, -1.0f, -1.0f,
      1.0f,  1.0f, -1.0f,
      -1.0f,  1.0f, -1.0f,

      -1.0f, -1.0f,  1.0f,
      -1.0f, -1.0f, -1.0f,
      -1.0f,  1.0f, -1.0f,
      -1.0f,  1.0f, -1.0f,
      -1.0f,  1.0f,  1.0f,
      -1.0f, -1.0f,  1.0f,

      1.0f, -1.0f, -1.0f,
      1.0f, -1.0f,  1.0f,
      1.0f,  1.0f,  1.0f,
      1.0f,  1.0f,  1.0f,
      1.0f,  1.0f, -1.0f,
      1.0f, -1.0f, -1.0f,

      -1.0f, -1.0f,  1.0f,
      -1.0f,  1.0f,  1.0f,
      1.0f,  1.0f,  1.0f,
      1.0f,  1.0f,  1.0f,
      1.0f, -1.0f,  1.0f,
      -1.0f, -1.0f,  1.0f,

      -1.0f,  1.0f, -1.0f,
      1.0f,  1.0f, -1.0f,
      1.0f,  1.0f,  1.0f,
      1.0f,  1.0f,  1.0f,
      -1.0f,  1.0f,  1.0f,
      -1.0f,  1.0f, -1.0f,

      -1.0f, -1.0f, -1.0f,
      -1.0f, -1.0f,  1.0f,
      1.0f, -1.0f, -1.0f,
      1.0f, -1.0f, -1.0f,
      -1.0f, -1.0f,  1.0f,
      1.0f, -1.0f,  1.0f
    };
    this->vertices.insert(vertices.end(), &skyboxVertices[0], &skyboxVertices[108]);

    // Load Skybox texture filenames
    this->faces.push_back("models/skybox0/right.jpg");
    this->faces.push_back("models/skybox0/left.jpg");
    this->faces.push_back("models/skybox0/top.jpg");
    this->faces.push_back("models/skybox0/bottom.jpg");
    this->faces.push_back("models/skybox0/front.jpg");
    this->faces.push_back("models/skybox0/back.jpg");
    // this->cubemapTexture = loadCubemap(this->faces);
    this->cubemapTexture = loadCubemap();
    // setup the mesh
    this->setupMesh();
  }

  // Destructor:???
  //~Skybox();

  void Draw(Shader shader)
  {
    glDepthMask(GL_FALSE);// Remember to turn depth writing off
    // Draw mesh
    glBindVertexArray(this->VAO);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, this->cubemapTexture);
    glDrawArrays(GL_TRIANGLES, 0, this->vertices.size()/3);
    glBindVertexArray(0);

    glDepthMask(GL_TRUE);
  }
private:
  // Setup skybox VAO
  GLuint VAO, VBO;

  void setupMesh() {
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);
    // Bind VAO
    glBindVertexArray(this->VAO);
    // Bind VBO to VAO and buffer vertex data
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
    glBufferData(GL_ARRAY_BUFFER, this->vertices.size()*sizeof(GLfloat),
    &this->vertices[0], GL_STATIC_DRAW);

    // Set vertex attribute pointers
    // Vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    // release VAO
    glBindVertexArray(0);
  }

  // Loads a cubemap texture from 6 individual texture faces
  // Order should be:
  // +X (right)
  // -X (left)
  // +Y (top)
  // -Y (bottom)
  // +Z (front)
  // -Z (back)
  // GLuint loadCubemap(std::vector<const GLchar*> faces)
  GLuint loadCubemap() {
    GLuint textureID;
    glGenTextures(1, &textureID);

    int width,height;
    unsigned char* image;

    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
    for(GLuint i = 0; i < this->faces.size(); i++)
    {
      image = SOIL_load_image(this->faces[i], &width, &height, 0, SOIL_LOAD_RGB);
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
      SOIL_free_image_data(image);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return textureID;
  }
  // cleanup skybox assets
  // glDeleteVertexArrays(1, &skyboxVAO);
  // glDeleteBuffers(1, &skyboxVBO);
};
