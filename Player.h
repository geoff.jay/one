#pragma once

// std includes
#include <queue>   // queue provides FIFO access
// GL includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Define abstract player movement operations
enum Player_Movement {
  pFORWARD,
  pBACKWARD,
  pLEFT,
  pRIGHT,
  pUP,
  pDOWN,
  pBOOST
};

// Default player movement values
const GLfloat pYAW         = 0.0f;
const GLfloat pPITCH       = 0.0f;
const GLfloat pSPEED       = 40.0f;
const GLfloat pSENSITIVITY = 0.15f;
const GLfloat pZOOM        = 1.0f;   // 1 radians

// Abstract camera class that processes input, calculates Euler angles and appropriate glm vectors and matrices
class Player
{
public:
  // Player Attributes
  glm::vec3 Position;
  glm::vec3 Front;
  glm::vec3 Up;
  glm::vec3 Right;
  glm::vec3 WorldUp;
  // Euler angles
  GLfloat Yaw;
  GLfloat Pitch;
  // Model tilt angles
  GLfloat dYaw;
  GLfloat dPitch;
  // std::queue<GLfloat> lastYaw;
  // std::queue<GLfloat> lastPitch;
  GLfloat lastYaw;
  GLfloat lastPitch;

  // Camera options
  GLfloat MovementSpeed;
  GLfloat MouseSensitivity;
  GLfloat Zoom;

  // Constructor: vector style input
  Player(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = pYAW, GLfloat pitch = pPITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(pSPEED), MouseSensitivity(pSENSITIVITY), Zoom(pZOOM)
  {
    this->Position = position;
    this->WorldUp = up;
    this->Yaw = yaw;
    this->Pitch = pitch;
    // for (int i = 0; i<100; i++) {
    //     this->lastYaw.push(0.0f);
    //     this->lastPitch.push(0.0f);
    // }
    this->lastYaw = 0.0f;
    this->lastPitch = 0.0f;
    this->updatePlayerVectors();
  }
  // Constructor: scalar style input (not used)
  Player(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) : Front(glm::vec3(1.0f, 0.0f, 0.0f)), MovementSpeed(pSPEED), MouseSensitivity(pSENSITIVITY), Zoom(pZOOM)
  {
    this->Position = glm::vec3(posX, posY, posZ);
    this->WorldUp = glm::vec3(upX, upY, upZ);
    this->Yaw = yaw;
    this->Pitch = pitch;
    this->updatePlayerVectors();
  }

  // Compute and return view matrix
  glm::mat4 getViewMatrix() {
    glm::mat4 mIdent = glm::mat4();
    glm::mat4 mRotZ = glm::rotate( mIdent, -glm::radians(this->Yaw + this->dYaw), this->Up );
    glm::mat4 mRotX = glm::rotate( mIdent, glm::radians(this->Pitch + this->dPitch), this->Right );
    glm::mat4 mTranslate = glm::translate( mIdent, this->Position - 10.0f*this->Up );

    glm::mat4 mTransform = mTranslate * mRotZ * mRotX;
    return mTransform;
  }

  // Process keyboard input
  void processKeys(Player_Movement direction, GLfloat deltaTime) {
    GLfloat velocity = this->MovementSpeed * deltaTime;
    if (direction == pFORWARD)
      this->Position += this->Front * velocity;
    if (direction == pBACKWARD)
      this->Position -= this->Front * velocity;
    if (direction == pLEFT)
      this->Position -= this->Right * velocity;
    if (direction == pRIGHT)
      this->Position += this->Right * velocity;
    if (direction == pUP)
      this->Position += this->Up * velocity;
    if (direction == pDOWN)
      this->Position -= this->Up * velocity;
    if (direction == pBOOST) {
        this->MovementSpeed = 2.5f * pSPEED;
    } else {
        this->MovementSpeed = pSPEED;
    }
    // this->dYaw = 0.0f;
    // this->dPitch = 0.0f;
  }

  // Process mouse input
  void processMouseMove(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true) {
    xoffset *= this->MouseSensitivity;
    yoffset *= this->MouseSensitivity;

    // this->Yaw   -= xoffset;
    this->Yaw = glm::mod(Yaw + xoffset, 360.f);
    this->Pitch += yoffset;

    // Make sure the screen doesn't flip at Pitch limits
    if (constrainPitch) {
      if (this->Pitch > 89.0f) this->Pitch = 89.0f;
      if (this->Pitch < -89.0f) this->Pitch = -89.0f;
    }

    // update the model rotation queue
    // this->lastYaw.push(xoffset);
    // this->lastPitch.push(yoffset);

    // calculate the current orientation based on recent steering
    this->dYaw = this->dYaw + 7*xoffset - 7*this->lastYaw;
    this->dPitch = this->dPitch + 17*yoffset - 17*this->lastPitch;
    this->lastYaw = xoffset;
    this->lastPitch = yoffset;

    // outputs
    // cout << this->Yaw << " : " << this->Pitch << endl;
    // if (xoffset > 0.0f)
    //     this->dYaw = 20.0f;
    // if (xoffset < 0.0f)
    //     this->dYaw = -20.0f;
    // if (yoffset > 0.0f)
    //     this->dPitch = 20.0f;
    // if (yoffset < 0.0f)
    //     this->dPitch = -20.0f;

    // Apply rotations; update Front/Right/Up vectors
    this->updatePlayerVectors();
  }

  // Process mouse scroll-wheel input
  // 1deg = 0.01745rad, 45deg = 0.7854
  void processMouseScroll(GLfloat yoffset) {
    if (this->Zoom >= 0.1745f && this->Zoom <= 1.7854f)
      this->Zoom -= 0.1 * yoffset;
    if (this->Zoom <= 0.1745f)
      this->Zoom = 0.1745f;
    if (this->Zoom >= 1.7854f)
      this->Zoom = 1.7854f;
  }

private:
  // Calculate Camera view vectors from updated Euler angles
  void updatePlayerVectors() {
    // Front
    glm::vec3 front;
    front.x = cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
    front.y = sin(glm::radians(this->Pitch));
    front.z = sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
    this->Front = glm::normalize(front);
    // Right
    this->Right = glm::normalize(glm::cross(this->Front, this->WorldUp));
    // Up
    this->Up    = glm::normalize(glm::cross(this->Right, this->Front));
  }
};
