# Introduction

Initial attempt at space exploration Game Engine.

Developed in OpenGL and C++

# Dependencies

## Required Libraries:<br>
- GLEW
- GLFW
- SOIL
- ASSIMP

### On Ubuntu 14.04

```bash
sudo apt-get install libglew-dev libsoil1 libsoil-dev libassimp3 libassimp-dev
```

### On Fedora 22 - 25

```bash
sudo dnf install glfw-devel glew-devel SOIL-devel assimp-devel glm-devel
```

### To get GLFW3

```bash
sudo add-apt-repository ppa:keithw/glfw3<br>
sudo apt-get update<br>
sudo apt-get install libglfw3 libglfw3-dev
```
